from collections import deque
import networkx as nx
from matplotlib import pyplot as plt

class Graph(object):
    """A graph object, stored as an adjacency dictionary. Each node in the
    graph is a key in the dictionary. The value of each key is a list of the
    corresponding node's neighbors.

    Attributes:
        dictionary: the adjacency list of the graph.
    """

    def __init__(self, adjacency):
        """Store the adjacency dictionary as a class attribute."""
        self.dictionary = adjacency


    def __str__(self):
        """String representation: a view of the adjacency dictionary.

        Example:
            >>> test = {'A':['B'], 'B':['A', 'C',], 'C':['B']}
            >>> print(Graph(test))
            A: B
            B: A; C
            C: B
        """
        #print("I'm going into the function")
        al = ""
        for key in self.dictionary:
            final = str(key)+": "
            for i in range(len(self.dictionary[key])-1):
                final = final + str(self.dictionary[key][i])+"; "
            length = len(self.dictionary[key])-1
            final = final + str(self.dictionary[key][length])+"\n"
            al = al + final
        return al



    def traverse(self, start):
        """Begin at 'start' and perform a breadth-first search until all
        nodes in the graph have been visited. Return a list of values,
        in the order that they were visited.

        Parameters:
            start: the node to start the search at.

        Returns:
            the list of visited nodes (in order of visitation).

        Raises:
            ValueError: if 'start' is not in the adjacency dictionary.

        Example:
            >>> test = {'A':['B'], 'B':['A', 'C',], 'C':['B']}
            >>> Graph(test).traverse('B')
            ['B', 'A', 'C']
        """
        to_visit = deque()
        visited = []
        marked = set()
        current = start
        if start not in self.dictionary:
            raise ValueError(str(start) + " is not in the adjacency matrix")

        to_visit.append(start)
        marked.add(start)
        while current != "":
            for neighbor in self.dictionary[current]:
                #print("the current is",current)
                #print("The current neighbor is", neighbor)
                #print("to_visit is",to_visit)
                #print("Visited is",visited)
                #print("Marked is", marked)
                if neighbor not in marked:
                    #print("Went into the if meaning neighbor was added to_visit and marked")
                    to_visit.append(neighbor)
                    marked.add(neighbor)
                #print("\n")
            #print("This is where it pops")
            if len(to_visit) != 0:
                current = to_visit.popleft()
                visited.append(current)
            else:
                current = ""

            #if len(to_visit) == 0:
            #    to_visit.append("Joker")

        return visited


    def shortest_path(self, start, target):
        """Begin at the node containing 'start' and perform a breadth-first
        search until the node containing 'target' is found. Return a list
        containg the shortest path from 'start' to 'target'. If either of
        the inputs are not in the adjacency graph, raise a ValueError.

        Parameters:
            start: the node to start the search at.
            target: the node to search for.

        Returns:
            A list of nodes along the shortest path from start to target,
                including the endpoints.

        Example:
            >>> test = {'A':['B', 'F'], 'B':['A', 'C'], 'C':['B', 'D'],
            ...         'D':['C', 'E'], 'E':['D', 'F'], 'F':['A', 'E', 'G'],
            ...         'G':['A', 'F']}
            >>> Graph(test).shortest_path('A', 'G')
            ['A', 'F', 'G']
        """
        to_visit = deque()
        visited = []
        marked = set()
        current = start
        previous = start
        paths={}

        if start not in self.dictionary:
            raise ValueError(str(start) + " is not in the adjacency matrix")
        elif target not in self.dictionary:
            raise ValueError(str(target) + " is not in the adjacency matrix")

        b=False
        visited.append(current)
        marked.add(start)
        while current != target:
            for neighbor in self.dictionary[current]:
                if neighbor not in marked:
                    to_visit.append(neighbor)
                    marked.add(neighbor)
                    paths[neighbor] = current
                    if neighbor == target:
                        b = True
            if b == True:
                break
            current = to_visit.popleft()
            visited.append(current)

        l = []
        l.append(target)
        while target != start:
            l.append(paths[target])
            target = paths[target]

        l.reverse()
        return l


def convert_to_networkx(dictionary):
    """Convert 'dictionary' to a networkX object and return it."""
    nx_graph = nx.Graph()
    for k in dictionary:
        for l in dictionary[k]:
            nx_graph.add_edge(k, l)

    return nx_graph



def parse(filename="movieData.txt"):
    """Generate an adjacency dictionary where each key is
    a movie and each value is a list of actors in the movie.
    """

    # open the file, read it in, and split the text by '\n'
    with open(filename, 'r') as movieFile:
        moviesList = movieFile.read().split('\n')
    graph = dict()

    # for each movie in the file,
    for movie in moviesList:
        # get movie name and list of actors
        names = movie.split('/')
        title = names[0]
        graph[title] = []
        # add the actors to the dictionary
        for actor in names[1:]:
            graph[title].append(actor)

    return graph

class BaconSolver(object):
    """Class for solving the Kevin Bacon problem."""


    def __init__(self, filename="movieData.txt"):
        """Initialize the networkX graph with data from the specified
        file. Store the graph as a class attribute. Also store the collection
        of actors in the file as an attribute.
        """
        self.dictionary = parse(filename)
        self.nx_graph = nx.Graph(self.dictionary)


    def path_to_bacon(self, start, target="Bacon, Kevin"):
        """Find the shortest path from 'start' to 'target'."""
        if start not in self.nx_graph or target not in self.nx_graph:
            raise ValueError("One of the end points not in the graph")
        if nx.has_path(self.nx_graph,start,target):
            return(nx.shortest_path(self.nx_graph,start,target))
        else:
            return []


    def bacon_number(self, start, target="Bacon, Kevin"):
        """Return the Bacon number of 'start'."""
        path = self.path_to_bacon(start,target)
        if path == []:
            return 0
        else:
            length = (len(path)-1)/2
        return length


    def average_bacon(self, target="Bacon, Kevin"):
        """Calculate the average Bacon number in the data set.
        Note that actors are not guaranteed to be connected to the target.

        Parameters:
            target (str): the node to search the graph for.

        Returns:
            (float): the average (finite) Bacon number
            (int): the number of actors not connected to the target.
        """
        total = 0
        num_actors = 0
        actors = set()
        for i in self.dictionary:
            for k in range(len(self.dictionary[i])-1):
                actors.add(self.dictionary[i][k])

        for m in actors:
            if nx.has_path(self.nx_graph,m,target):
                temp = self.bacon_number(m)
            if 0 != temp:
                total += temp
                num_actors += 1
                actors.add(self.dictionary[i][k])
        print(num_actors)
        print(total)
        return total/float(num_actors)

if __name__ == "__main__":
    my_dictionary = {'A':['C', 'B'], 'C':['A'], 'B':['A']}
    temp = {'A':['B'],'B':['A','C'],'C':['A','B','D'],'D':['A','C','E'],'E':['A']}

    test = {'A':['B', 'F'],
                     'B':['A', 'C'],
                     'C':['B', 'D'],
                     'D':['C', 'E'],
                     'E':['D', 'F'],
                     'F':['A', 'E', 'G'],
                     'G':['A', 'F']}
    g = Graph(temp)
    #print(g)
    #print(g.traverse('H'))
    #print(g.shortest_path('A','E'))
    #nx_graph = convert_to_networkx(g.dictionary)
    #nx.draw(nx_graph)
    #plt.show()
    #movie_graph = BaconSolver("movieData.txt")
    #print(movie_graph.path_to_bacon("Jackson, Samuel L.")
