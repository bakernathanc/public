import numpy as np
from scipy import linalg as la
from scipy import stats
from matplotlib import pyplot as plt

def ball_volume(n, N=10000):
    """Estimate the volume of the n-dimensional unit ball.

    Parameters:
        n (int): The dimension of the ball. n=2 corresponds to the unit circle,
            n=3 corresponds to the unit sphere, and so on.
        N (int): The number of random points to sample.

    Returns:
        (float): An estimate for the volume of the n-dimensional unit ball.
    """

    points = np.random.uniform(-1,1,(n,int(N)))
    lengths = la.norm(points,axis=0)
    num_within = np.count_nonzero(lengths < 1)

    return (2**n) * (num_within / N)




def mc_integrate1d(f, a, b, N=10000):
    """Approximate the integral of f on the interval [a,b].

    Parameters:
        f (function): the function to integrate. Accepts and returns scalars.
        a (float): the lower bound of interval of integration.
        b (float): the lower bound of interval of integration.
        N (int): The number of random points to sample.

    Returns:
        (float): An approximation of the integral of f over [a,b].


    """

    points = np.random.uniform(a,b,N)
    lengths = la.norm(points,axis=0)
    num_within = np.count_nonzero(lengths < 1)

    vol = (b-a)*(1/N)*sum(f(points))

    return vol



def mc_integrate(f, mins, maxs, N=10000):
    """Approximate the integral of f over the box defined by mins and maxs.

    Parameters:
        f (function): The function to integrate. Accepts and returns
            1-D NumPy arrays of length n.
        mins (list): the lower bounds of integration.
        maxs (list): the upper bounds of integration.
        N (int): The number of random points to sample.

    Returns:
        (float): An approximation of the integral of f over the domain.

    """
    n = len(mins)
    #print("n",n)
    points = np.random.uniform(0,1,(n,N))
    maxs = np.array(maxs)
    maxs = np.vstack(maxs)
    mins = np.array(mins)
    mins = np.vstack(mins)

    vol = 1
    #scaling everything
    temp = maxs-mins
    points = points * temp + mins
    #for i in range(n):
    #    points[i] = points[i] * temp[i] + mins[i]

    #calculating the volume
    vol = np.prod(maxs-mins)
    #calculating the f(xi)


    points = points.T
    fpoints = [f(x) for x in points]
    #for i in range(N):
    #    fpoints.append(f(points[:,i]))

    return vol * (1/N) * np.sum(fpoints)


def prob4():
    """Let n=4 and Omega = [-3/2,3/4]x[0,1]x[0,1/2]x[0,1].
    - Define the joint distribution f of n standard normal random variables.
    - Use SciPy to integrate f over Omega.
    - Get 20 integer values of N that are roughly logarithmically spaced from
        10**1 to 10**5. For each value of N, use mc_integrate() to compute 25
        estimates of the integral of f over Omega with N samples, and average
        the estimates together. Compute the relative error of each average.
    - Plot the relative error against the sample size N on a log-log scale.
        Also plot the line 1 / sqrt(N) for comparison.
    """


    n = 4
    mins = [-3/2,0,0,0]
    maxs = [3/4,1,1/2,1]
    f = lambda x: (1/(2*np.pi)**(2))*np.exp(-(x.T @ x)/2)

    cov = np.eye(4)
    mean = np.zeros(4)

    exact = stats.mvn.mvnun(mins,maxs,mean,cov)[0]

    N = np.logspace(1,5,20)
    things = []

    for n in N:
        temp = []
        for i in range(25):
            temp.append(mc_integrate(f, mins, maxs, int(n)))
        mine = sum(temp)/len(temp)
        things.append((abs(exact-mine))/abs(exact))

    plt.loglog(N,things)
    domain = np.logspace(1,6,100)
    g = lambda d: 1/np.sqrt(d)
    plt.loglog(domain,g(domain))

    plt.show()

if __name__ == "__main__":
    n=4
    N = 10000
    #print(ball_volume(n, N))

    #f = lambda x: 1/x
    a=1
    b=10

    #print(mc_integrate1d(f, a, b, N=10000))

    #mins = [0,0]
    #maxs = [1,1]
    #f = lambda x: x[0]**2 + x[1]**2

    mins = [1,-2]
    maxs = [3,1]
    f = lambda x: 3*x[0] - 4*x[1] + x[1]**2

    #mins = [-1,-2,-3,-4]
    #maxs = [1,2,3,4]
    #f = lambda x: x[0]+x[1]-x[3]*(x[2]**2)

    N=10000
    #print(mc_integrate(f, mins, maxs, N))

    #prob4()
