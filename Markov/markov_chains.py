# markov_chains.py
"""Volume II: Markov Chains.
<Name>
<Class>
<Date>
"""

import numpy as np
from numpy import linalg as la

# Problem 1
def random_chain(n):
    """Create and return a transition matrix for a random Markov chain with
    'n' states. This should be stored as an nxn NumPy array.
    """
    A = np.eye(n)
    for i in range(n):
        l = np.random.random(n)
        s = sum(l)
        l = l/s
        A[i] = l
    return A.transpose()
    #raise NotImplementedError("Problem 1 Incomplete")


# Problem 2
def forecast(days):
    """Forecast tomorrow's weather given that today is hot."""
    transition = np.array([[0.7, 0.6], [0.3, 0.4]])
    if type(days) is not int:
        raise ValueError("Days isn't an integer")
    final = []
    for i in range(days):
        final.append(np.random.binomial(1,transition[1,0]))
    # Sample from a binomial distribution to choose a new state.
    return final

# Problem 3
def four_state_forecast(days):
    """Run a simulation for the weather over the specified number of days,
    with mild as the starting state, using the four-state Markov chain.
    Return a list containing the day-by-day results, not including the
    starting day.

    Examples:
        >>> four_state_forecast(3)
        [0, 1, 3]
        >>> four_state_forecast(5)
        [2, 1, 2, 1, 1]
    """
    transition = np.array([[0.5,0.3,0.1,0],[0.3,0.3,0.3,0.3],
            [0.2,0.3,0.4,0.5],[0,0.1,0.2,0.2]])
    temp = []
    final = []
    today_temp = 1
    for i in range(days):
        temp = np.random.multinomial(1,transition[today_temp])
        today_temp =np.argmax(temp)
        final.append(today_temp)
    return final

    #raise NotImplementedError("Problem 3 Incomplete")


# Problem 4
def steady_state(A, tol=1e-12, N=40):
    """Compute the steady state of the transition matrix A.

    Inputs:
        A ((n,n) ndarray): A column-stochastic transition matrix.
        tol (float): The convergence tolerance.
        N (int): The maximum number of iterations to compute.

    Raises:
        ValueError: if the iteration does not converge within N steps.

    Returns:
        x ((n,) ndarray): The steady state distribution vector of A.
    """
    #it's nxn
    #A = np.array([[.7,.6],[.3,.4]])
    n = A.shape[0]
    #ox = np.array([[.8],[.2]])
    #xk1 = np.array([[2/3],[1/3]])
    ox = np.random.rand(n,1)
    xk1=np.random.rand(n,1)
    #print("x is",ox)
    xk = np.dot(A,ox)
    #print("Ax is",xk)
    fail = 1
    for k in range(2,N):
        Ak = np.linalg.matrix_power(A, k)
        xk = np.dot(Ak,ox)
        #print("next iteration is",xk)
        if la.norm(xk1-xk) < tol:
            #print("seems to be working")
            fail = 0
            break
        xk1 = xk
    if fail == 1:
        raise ValueError("It seems that A^k isn't going to converge")


    return xk
    #raise NotImplementedError("Problem 4 Incomplete")


# Problems 5 and 6
class SentenceGenerator(object):
    """Markov chain creator for simulating bad English.

    Attributes:
        (what attributes do you need to keep track of?)

    Example:
        >>> yoda = SentenceGenerator("Yoda.txt")
        >>> print(yoda.babble())
        The dark side of loss is a path as one with you.
    """
    def initializer_read_in(filename):
        ifile = open(infile, 'rt',encoding='UTF-8')
        lines = []
        for line in ifile:
            lines.append(line.split(' '))
        words = {'$tart':0}
        count = 1
        for word in lines:
            if word in words:
                pass
            else:
                words[word] = count
            count +=1
        words['$top'] = count
        return words

    def __init__(self, filename):
        """Read the specified file and build a transition matrix from its
        contents. You may assume that the file has one complete sentence
        written on each line.
        """
        def initializer_read_in(filename):
            ifile = open(filename, 'rt',encoding='UTF-8')
            lines = []
            for line in ifile:
                lines.append(line.split(' '))
            words = {'$tart':0}
            count = 1
            line_count=0
            for line in lines:
                word_count = 0
                for word in line:
                    word = word.split('\n')[0]
                    lines[line_count][word_count] = word
                    if word not in words:
                        words[word] = count
                        count +=1

                    word_count += 1
                line_count += 1
            words['$top'] = count
            return words,lines

        def create_matrix(words,lines):

            """While going through each sentecne
                for going through each word"""
            A = np.zeros([len(words),len(words)])
            #print("This is lines",lines)
            #print("This is words",words)
            for line in lines:
                previous_word = '$tart'
                wordcount = 0
                for word in line:
                    if wordcount == len(line)-1:
                        #last word in the sentence
                        n = words[previous_word]
                        m = words[word]
                        A[m,n] +=1
                        #Then needs to transition to stop
                        previous_word = word
                        word = '$top'
                        n = words[previous_word]
                        m = words[word]
                        A[m,n] +=1
                    else:
                        #otherwise
                        n = words[previous_word]
                        m = words[word]
                        A[m,n] +=1
                    previous_word = word
                    wordcount +=1

            m,n = A.shape
            A[m-1,n-1] += 1
            return(A)

        def normalize(A):
            pass
            m,n = A.shape
            s=A.sum(axis=0)
            for i in range(m):
                for j in range(n):
                    A[j,i] = A[j,i]/s[i]
            return(A)

        words,lines = initializer_read_in(filename)
        A = np.zeros([len(words)-1,len(words)-1])
        A = create_matrix(words,lines)
        A = normalize(A)

        self.transition = A
        self.words = words
    def babble(self):
        """Begin at the start sate and use the strategy from
        four_state_forecast() to transition through the Markov chain.
        Keep track of the path through the chain and the corresponding words.
        When the stop state is reached, stop transitioning and terminate the
        sentence. Return the resulting sentence as a single string.
        """

        current_word = self.words['$tart']
        n = self.transition.shape[0]
        final = [current_word]
        result = np.random.multinomial(1,self.transition[:,final[-1]])
        while result.nonzero()[0][0] != n-1:
            #print("am I goign crazy")
            final.append(result.nonzero()[0][0])
            result = np.random.multinomial(1,self.transition[:,final[-1]])
            #current_word = np.argmax(temp)
            #final.append(current_word)
        to_print = []
        for index_final in final:
            for word in self.words:
                if self.words[word] == index_final and word != '$tart':
                    to_print.append(word)
        #print(to_print)
        #print(' '.join(to_print))
        return ' '.join(to_print)

        #raise NotImplementedError("Problem 6 Incomplete")
if __name__ == "__main__":
    pass
    #n = 2
    #A = random_chain(n)

    #print("whereas the steady state is \n",steady_state(A))
    #print(random_chain(n))
    #print("This is the if print",four_state_forecast(n))
    #print(steady_state(A))
    #SG = SentenceGenerator("sam.txt")
    #print(SG.babble())
    print(forecast(5))
