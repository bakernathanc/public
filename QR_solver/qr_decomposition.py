import numpy as np
from scipy import linalg as la


def qr_gram_schmidt(A):
    """Compute the reduced QR decomposition of A via Modified Gram-Schmidt.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,n) ndarray): An orthonormal matrix.
        R ((n,n) ndarray): An upper triangular matrix.
    """
    m,n = A.shape
    Q = A.copy()
    #Qt = Q.transpose()
    R = np.zeros((n,n))
    for i in range(n):
        R[i][i] = la.norm(Q[:,i])
        Q[:,i] = Q[:,i]/R[i][i]
        for j in range(i+1, n):
            R[i][j] = np.dot(Q[:,j],Q[:,i])
            Q[:,j] -= R[i][j]*Q[:,i]

    return Q,R






def abs_det(A):
    """Use the QR decomposition to efficiently compute the absolute value of
    the determinant of A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) the absolute value of the determinant of A.
    """
    Q,R = qr_gram_schmidt(A)
    m,n = A.shape
    det = 1
    for i in range(m):
        det = det * np.abs(R[i][i])
    return det



def solve(A, b):
    """Use the QR decomposition to efficiently solve the system Ax = b.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.
        b ((n, ) ndarray): A vector of length n.

    Returns:
        x ((n, ) ndarray): The solution to the system Ax = b.
    """
    Q,R = qr_gram_schmidt(A)
    Qt = Q.transpose()
    n,n = A.shape
    y = np.dot(Qt,b)
    x = np.zeros(n)

    for l in reversed(range(n)):
        x[l] = (1/float(R[l,l]))*(float(y[l])-sum(R[l,l:n]*x[l:]))

    return x





def qr_householder(A):
    """Compute the full QR decomposition of A via Householder reflections.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,m) ndarray): An orthonormal matrix.
        R ((m,n) ndarray): An upper triangular matrix.
    """
    sign = lambda x: 1 if x >= 0 else -1

    m,n = A.shape
    R = A.copy()
    Q = np.eye(m)
    for k in range(0,n):
        u = R[k:,k].copy()
        u[0] = u[0] + sign(u[0])*la.norm(u)
        u = u / float(la.norm(u))
        R[k:,k:] = R[k:,k:] - np.outer(2*u,(np.dot(u.transpose(),R[k:,k:])))
        Q[k:,:] = Q[k:,:] - 2*np.outer(u,np.dot(u.transpose(),Q[k:,:]))

    return Q.transpose(), R





def hessenberg(A):
    """Compute the Hessenberg form H of A, along with the orthonormal matrix Q
    such that A = QHQ^T.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.

    Returns:
        H ((n,n) ndarray): The upper Hessenberg form of A.
        Q ((n,n) ndarray): An orthonormal matrix.
    """
    sign = lambda x: 1 if x >= 0 else -1

    m,n = A.shape
    if m != n:
        raise ValueError("A is not an (n,n) matrix")

    m,n = A.shape
    H = A.copy()
    Q = np.eye(m)
    for k in range(0,n-2):
        u = H[k+1:,k].copy()
        u[0] += sign(u[0])*la.norm(u)
        u = u/float(la.norm(u))
        #print(u.shape)
        #print H.shape
        temp1 = np.dot(u.transpose(),H[k+1:,k:])
        #print(temp1.shape)
        #temp = np.dot(u,np.dot(u.transpose(),H[k+1:,k:]))
        H[k+1:,k:] -= 2*np.outer(u,np.dot(u.transpose(),H[k+1:,k:]))
        H[:,k+1:] -= 2*np.outer(np.dot(H[:,k+1:],u),u.transpose())
        Q[k+1:,:] -= 2*np.outer(u,np.dot(u.transpose(),Q[k+1:,:]))

    return H,Q.transpose()



if __name__ == "__main__":
    A = np.random.random((6,6))
    """
    A = np.random.random((6,4))
    Q,R = la.qr(A, mode="economic") # Use mode="economic" for reduced QR.
    Qm,Rm = qr_gram_schmidt(A)
    print(np.allclose(np.triu(R), R))
    print(np.allclose(np.dot(Q.T, Q), np.identity(4)))
    print(np.allclose(np.dot(Qm,Rm),A))
    """

    #problem2
    """
    print(abs_det(A))
    print(np.linalg.det(A))"""

    """
    b = np.random.random(6)
    print(solve(A,b))
    print(la.solve(A,b))"""

    """
    qrh = qr_householder(A)
    Q,R = la.qr(A)
    print(np.allclose(Q.dot(R),A))"""

    """
    Hm, Qtm = hessenberg(A)
    H,Q = la.hessenberg(A, calc_q = True)
    print(np.allclose(np.triu(H,-1),H))
    print(np.allclose(np.dot(np.dot(Q,H),Q.T),A))"""
