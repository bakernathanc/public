
from numpy import linalg as la
import numpy as np
import time
import math
from numba import jit
from numba import int64, double
from matplotlib import pyplot as plt


def max_path(filename="triangle.txt"):
    """Find the maximum vertical path in a triangle of values."""
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
    def path_sum(r, c, total):
        """Recursively compute the max sum of the path starting in row r
        and column c, given the current total.
        """
        total += data[r][c]
        if r == len(data) - 1:          # Base case.
            return total
        else:                           # Recursive case.
            return max(path_sum(r+1, c,   total),   # Next row, same column
                       path_sum(r+1, c+1, total))   # Next row, next column

    return path_sum(0, 0, 0)            # Start the recursion from the top.

def max_path_fast(filename="triangle.txt"):
    """Find the maximum vertical path in a triangle of values."""
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
    rows = len(data)-1
    d = data

    def next_row(row,d):
        if row == 0:
            return d
        for i in range(row-1,-1,-1):
            greater = d[row][i]+d[row-1][i]
            if d[row][i+1]+d[row-1][i]>greater:
                d[row-1][i] = d[row][i+1]+d[row-1][i]
            else:
                d[row-1][i] = greater
        return d

    for r in range(rows,-1,-1):
        d = next_row(r,d)

    return d[0][0]



def primes(N):
    """Compute the first N primes."""
    primes_list = []
    current = 2
    while len(primes_list) < N:
        isprime = True
        for i in range(2, current):     # Check for nontrivial divisors.
            if current % i == 0:
                isprime = False
        if isprime:
            primes_list.append(current)
        current += 1
    return primes_list

def primes_fast(N):
    """Compute the first N primes."""

    p = [2]
    current = 3
    num_primes = 1
    isprime = True
    while num_primes < N:
        sqrtcurr = math.sqrt(current)
        for i in p:
            if i > sqrtcurr:
                break
            if current % i == 0:
                isprime = False
                break
        if isprime:
            p.append(current)
            num_primes += 1
        else:
            isprime = True
        current += 2
    return p





def nearest_column(A, x):
    """Find the index of the column of A that is closest to x.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    distances = []
    for j in range(A.shape[1]):
        distances.append(np.linalg.norm(A[:,j] - x))
    return np.argmin(distances)

def nearest_column_fast(A, x):
    """Find the index of the column of A that is closest in norm to x.
    Refrain from using any loops or list comprehensions.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    return np.argmin(la.norm((A.T-x).T,axis=0))


def name_scores(filename="names.txt"):
    """Find the total of the name scores in the given file."""
    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))
    total = 0
    for i in range(len(names)):
        name_value = 0
        for j in range(len(names[i])):
            alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            for k in range(len(alphabet)):
                if names[i][j] == alphabet[k]:
                    letter_value = k + 1
            name_value += letter_value
        total += (names.index(names[i]) + 1) * name_value
    return total

def name_scores_fast(filename='names.txt'):
    """Find the total of the name scores in the given file."""

    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))

    total = 0
    index = 1
    alphabet = {'A':1,'B':2,'C':3,'D':4,'E':5,'F':6,'G':7,'H':8,'I':9,'J':10,'K':11,'L':12,'M':13,'N':14,'O':15,'P':16,'Q':17,'R':18,'S':19,'T':20,'U':21,'V':22,'W':23,'X':24,'Y':25,'Z':26}
    for name in names:
        v = 0
        for letter in name:
            v += alphabet[letter]
        total += v*index
        index += 1
    return total



def fibonacci():
    """Yield the terms of the Fibonacci sequence with F_1 = F_2 = 1."""
    f1 = 1
    f2 = 1
    f3 = f1+f2
    yield f1
    yield f2
    yield f3
    f4 = f3+f2
    yield f4
    while True:
        f5 = f4+f3
        yield f5
        f3 = f4
        f4 = f5



def fibonacci_digits(N=1000):
    """Return the index of the first term in the Fibonacci sequence with
    N digits.

    Returns:
        (int): The index.
    """
    for i in fibonacci():
        if len(str(i))+1 > N:
            return i




def prime_sieve(N):
    """Yield all primes that are less than N."""

    numbers = [i+2 for i in range(N)]
    primes = []
    for j in numbers:
        numbers = [k for k in numbers if not k%j == 0]
        if not numbers:
            break
        yield numbers.pop(0)




def matrix_power(A, n):
    """Compute A^n, the n-th power of the matrix A."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product

@jit(nopython=True, locals=dict(A=double[:,:],m=int64,n=int64,row_totals=double[:],total=double))
def matrix_power_numba(A, n):
    """Compute A^n, the n-th power of the matrix A, with Numba optimization."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product



def npower(n=8):
    """Time matrix_power(), matrix_power_numba(), and np.linalg.matrix_power()
    on square matrices of increasing size. Plot the times versus the size.
    """
    A = np.random.random((2,2))
    matrix_power_numba(A,2)
    numba = []
    power = []
    num = []
    #matrix_power_numba(A,n)
    for i in range(2,n):
        #print(i)
        #print("fast")
        A = np.random.random((2**i,2**i))
        start = time.time()
        matrix_power_numba(A,i)
        numba.append(time.time()-start)

        #print("slow")
        start=time.time()
        matrix_power(A,i)
        power.append(time.time()-start)

        #print("numpy")
        start=time.time()
        la.matrix_power(A,n)
        num.append(time.time()-start)

    plt.loglog(power,'g',label="matrix power")
    plt.loglog(numba,'r',label="Numba")
    plt.loglog(num,'b',label="Numpy")
    plt.xlabel("Matrix Size")
    plt.ylabel("Time")
    plt.show()

    #raise NotImplementedError("Problem 7 Incomplete")


def temp(n=10):
    @jit(nopython=True, locals=dict(A=double[:,:], m=int64, n=int64, row_totals=double[:], total=double))
    def row_sum_numba(A):
        m,n = A.shape
        row_totals = np.empty(m)
        for i in range(m):
            total = 0
            for j in range(n):
                total += A[i,j]
            row_totals[i] = row_total
        return row_totals
    A = np.random.random((10,10))


if __name__ == "__main__":
    """print(max_path("triangle.txt"))
    start = time.time()
    max_path()
    end = time.time()
    print("The first is ",end-start)
    start = time.time()
    max_path_fast("triangle_large.txt")
    end = time.time()
    print("While the second is ", end-start)
    print(max_path_fast("triangle_large.txt"))"""
    """l = []
    for i in range(100):
        start = time.time()
        name_scores_fast()
        l.append(time.time()-start)
    print(sum(l)/len(l))
    """

    pass
    #print(primes(100))
    #start = time.time()
    #primes_fast(10000)
    #print(time.time()-start)
    #print(fibonacci_digits())
    start = time.time()
    p=[]
    i=0
    for i in prime_sieve(100000):
        pass
    end = time.time()
    print(end-start)
    #npower()
    #print(temp())
    #N = 10000
    #prime_sieve(N)
    #A = np.random.random((15,15))
    #x = np.random.random(15)
    #print(np.allclose(nearest_column(A,x),nearest_column_fast(A,x)))
